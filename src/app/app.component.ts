import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {NgbModal, ModalDismissReasons, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import { User } from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [NgbModalConfig, NgbModal]
})
export class AppComponent implements OnInit {
  title = 'newcode';
  closeResult = '';
  public userForm: FormGroup;
  public userList: Array<User> = [];
  public submitted = false;
  public selectedUserIndex:any = 0;
  constructor(config: NgbModalConfig, private modalService: NgbModal) {
    config.backdrop = 'static';
    config.keyboard = false;
   }
  ngOnInit() {
    this.userForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('',[Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
      address: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$'), Validators.minLength(10), Validators.maxLength(10)])
    });
    // add existing user
    this.fetchUsers();
  }

  get userFormControl() {
    return this.userForm.controls;
  }
  get email(){
    return this.userForm.get('email')
  }
  get phone(){
    return this.userForm.get('phone')
  }
  fetchUsers(): void{
    this.userList = [{
        name: 'John',
        email: 'jon@gmail.com',
        address: '1234 Main St',
        phone: '1234567890'
      },
      {
        name: 'kurian',
        email: 'kurian@gmail.com',
        address: '1234 ways St',
        phone: '1234567890'
      }];
    }
  addUser(user: any): void {
    try {
    this.submitted = true;
    if(user.valid){
      this.userList.push(user.value);
      this.submitted = false;
      this.modalService.dismissAll();
      this.userForm.reset();
    } 
    } catch (error) {
      console.log(error); 
    }
  }
  deleteUser(): void{
    console.log(this.selectedUserIndex);
    if(this.selectedUserIndex !== 0){
      this.userList.splice(this.userList.indexOf(this.selectedUserIndex), 1);
      this.modalService.dismissAll();
    }
  }

  open(content: any, index: any = 0):void {
    this.selectedUserIndex = index;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
