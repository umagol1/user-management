
export class User {
    name?: string;
    email?: string;
    phone?: string;
    address?: string;
    constructor(user: any) {
      {
        this.name = user.name || '';
        this.email = user.email || '';
        this.address = user.address || '';
        this.phone = user.phone || '';
    }
    }
  }
  